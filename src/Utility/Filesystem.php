<?php
namespace InstituteWeb\Tx\Utility;

use Symfony\Component\Finder\Finder;

/**
 * Class Filesystem
 * extends Symfony's Filesystem
 *
 * @package InstituteWeb\Tx\Utility
 */
class Filesystem extends \Symfony\Component\Filesystem\Filesystem
{
    /**
     * Copies given source folder to given destination
     *
     * @param string $source
     * @param string $destination
     * @return bool
     */
    public function copyDirectory($source, $destination)
    {
        $sourceFiles = (new Finder())
            ->in($source)
            ->files();

        if (empty($sourceFiles)) {
            return false;
        }
        foreach ($sourceFiles as $file) {
            $relativeFilePath = '';
            if ($file->getRelativePath()) {
                $relativeFilePath = DIRECTORY_SEPARATOR . $file->getRelativePath();
            }
            $relativeFilePath .= DIRECTORY_SEPARATOR . $file->getFilename();
            $this->copy(
                \InstituteWeb\Tx\Variable::templatePath('kickstart') . $relativeFilePath,
                $destination . $relativeFilePath
            );
        }
        return true;
    }
}
