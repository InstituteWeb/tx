<?php
namespace InstituteWeb\Tx\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class KickstartCommand
 * Creates new TYPO3 extensions based on a given template
 *
 * @package InstituteWeb\Tx\Command
 */
class KickstartCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('kickstart')
            ->setDescription('Creates new TYPO3 extension.')

            ->addArgument('path', InputArgument::OPTIONAL, 'Path where to create extension folder.', '.')

            ->addOption('extkey', 'e', InputOption::VALUE_OPTIONAL, 'The extension key of the new extension.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output); // ask for required options

        if (!$input->getOption('extkey')) {
            $input->setOption(
                'extkey',
                $this->ask('Please enter new extension key: ', $this->getPath()->getNewFolderPart())
            );
        }

        // Get destination (merge given extkey and last part of path, if the same)
        $destination = $this->getPath()->get();
        if ($input->getOption('extkey') != $this->getPath()->getNewFolderPart()) {
            $destination = $this->getPath()->get() . DIRECTORY_SEPARATOR . $input->getOption('extkey');
        }

        // Ask to continue
        $confirm = $this->askConfirmation('Do you want to copy files to "' . $destination . '"?');
        if (!$confirm) {
            return;
        }

        // Copy process
        $output->write('Copying... ');
        $filesystem = new \InstituteWeb\Tx\Utility\Filesystem();
        $filesystem->copyDirectory(
            \InstituteWeb\Tx\Variable::templatePath('kickstart'),
            $destination
        );
        $output->writeln('done.');

        // TODO: Replace placeholders
    }
}
