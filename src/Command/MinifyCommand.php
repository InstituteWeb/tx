<?php
namespace InstituteWeb\Tx\Command;

use InstituteWeb\Tx\Value\Path;
use Symfony\Component\Console;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class MinifyCommand
 * Minify and gzip css and js resources.
 *
 * @package InstituteWeb\Tx\Command
 */
class MinifyCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('minify')
            ->setDescription('Concatinate, minify and gzip css and js resources.')

            ->addArgument('paths', InputArgument::REQUIRED | InputArgument::IS_ARRAY, 'Path(s) to minify', [])
            ->addOption('gzip', 'z', InputOption::VALUE_NONE, 'Stores the file gzipped with appended .gzip')
            ->addOption('output', 'o', InputOption::VALUE_OPTIONAL, 'Optional filename to output, including file extension.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);

        // First check if given paths are existing and all from same type (css or js)
        /** @var Path[] $validPaths */
        $validPaths = [];
        $filesType = null;
        foreach ($this->getPaths() as $path) {
            if (!$path->isExisting()) {
                $output->writeln('Warning: Ignoring "' . $path . '", file not found.');
                continue;
            }

            if (is_null($filesType)) {
                $filesType = strtoupper($path->getFileExtension());
            }
            if (!is_null($filesType) && strtoupper($path->getFileExtension()) !== $filesType) {
                throw new Console\Exception\InvalidArgumentException(
                    'Please don\'t mix js and css! Just one file type per minifier call is allowed.'
                );
            }
            $validPaths[] = $path;
        }

        // if no valid files are left, exit with exception
        if (empty($validPaths)) {
            throw new Console\Exception\InvalidArgumentException(
                'No valid files given. Aborting.'
            );
        }

        // Build output path if not set in output option
        $output = $input->getOption('output');
        if (!$output) {
            $output = getcwd() . DIRECTORY_SEPARATOR . 'result.min.js';
            if (count($validPaths) === 1) {
                $file = pathinfo($validPaths[0]->get(), PATHINFO_FILENAME) . '.' . strtolower($filesType);
                $file = preg_replace('/(.*?)\.(.*)/i', '$1.min.$2', $file);
                $output = getcwd() . DIRECTORY_SEPARATOR . $file;
            }
        }

        // check if destination folder is writable
        if (!is_writable(dirname($output))) {
            throw new Console\Exception\RuntimeException('Destination "' . $output . '" is unwritable.');
        }

        // check if file exists
        if (file_exists($output) && !$this->askConfirmation('File "' . $output . '" exists already. Overwrite?')) {
            return;
        }

        // perform the magic of matthiasmullie/minifier and create instance of minifier (JS or CSS)
        $minifierClassName = '\\MatthiasMullie\\Minify\\'  . $filesType;
        /** @var \MatthiasMullie\Minify\Minify $minifier */
        $minifier = new $minifierClassName();
        foreach ($validPaths as $validPath) {
            $minifier->add($validPath->get());
        }

        $this->writeln('Minifying...');
        $minifier->minify($output);
        $this->writeln('Created "' . $output . '".');

        // create gzip file, if --gzip is set
        if ($input->getOption('gzip')) {
            $output .= '.gzip';
            $minifier->gzip($output);
            $this->writeln('Created "' . $output . '".');
        }

        $this->writeln('Done.');
    }
}
