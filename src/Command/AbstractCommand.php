<?php
namespace InstituteWeb\Tx\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

abstract class AbstractCommand extends Command
{
    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var OutputInterface
     */
    protected $output;


    protected function configure()
    {
    }

    /**
     * Ask for missing required options
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return null|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
    }

    /**
     * Returns the path from first argument of command call.
     * Using Path value object here.
     *
     * @return \InstituteWeb\Tx\Value\Path
     */
    public function getPath()
    {
        return new \InstituteWeb\Tx\Value\Path($this->input->getArgument('path'));
    }

    /**
     * Return array of paths from arguments of command call.
     * Using path value objects here.
     *
     * @return \InstituteWeb\Tx\Value\Path[]
     */
    public function getPaths()
    {
        $paths = [];
        foreach ($this->input->getArgument('paths') as $path) {
            $paths[] = new \InstituteWeb\Tx\Value\Path($path);
        }
        return $paths;
    }

    /**
     * Writes to output
     *
     * @param string|array $messages
     * @param bool $newline
     * @param int $options
     */
    protected function write($messages, $newline = false, $options = 0)
    {
        $this->output->write($messages, $newline, $options);
    }

    /**
     * Writes to output
     *
     * @param string|array $messages
     * @param int $options
     */
    protected function writeln($messages, $options = 0)
    {
        $this->output->writeln($messages, $options);
    }

    /**
     * Asks the user a question
     *
     * @param string $question
     * @param mixed|null $default
     * @param callable|null|bool $validator If false no validator called.
     * @return string The user input
     */
    protected function ask($question, $default = null, callable $validator = null)
    {
        $questionText = $question;
        if (!is_null($default)) {
            $questionText = rtrim($questionText, ': ');
            $questionText .= ' [' . $default . ']: ';
        }

        $questionInstance = new Question($questionText, $default);
        if (is_null($validator)) {
            $validator = function ($answer) {
                if (!$answer) {
                    throw new \RuntimeException('No value given');
                }
                return $answer;
            };
        }
        if ($validator === false) {
            $validator = null;
        }
        $questionInstance->setValidator($validator);

        /** @var \Symfony\Component\Console\Helper\QuestionHelper $questionHelper */
        $questionHelper = $this->getHelper('question');
        return $questionHelper->ask($this->input, $this->output, $questionInstance);
    }

    /**
     * @param string $question
     * @param bool $default true
     * @return bool The user input: true or false
     */
    protected function askConfirmation($question, $default = true)
    {
        $defaultText = $default ? 'Y/n' : 'y/N';
        $questionInstance = new ConfirmationQuestion($question . ' [' . $defaultText . '] ', $default, '/^yj/i');
        /** @var \Symfony\Component\Console\Helper\QuestionHelper $questionHelper */
        $questionHelper = $this->getHelper('question');
        return $questionHelper->ask($this->input, $this->output, $questionInstance);
    }

    /**
     * Asks the user. Its input is hidden (if supported from the system)
     *
     * @param string $question
     * @param mixed|null $default
     * @param callable|null|bool $validator If false no validator called.
     * @return string The user input
     */
    protected function askHidden($question, $default = null, callable $validator = null)
    {
        $questionInstance = new Question($question, $default);
        $questionInstance->setHidden(true);
        $questionInstance->setHiddenFallback(true);
        if (is_null($validator)) {
            $validator = function ($answer) {
                if (!$answer) {
                    throw new \RuntimeException('No value given');
                }
                return $answer;
            };
        }
        if ($validator === false) {
            $validator = null;
        }
        $questionInstance->setValidator($validator);

        /** @var \Symfony\Component\Console\Helper\QuestionHelper $questionHelper */
        $questionHelper = $this->getHelper('question');
        return $questionHelper->ask($this->input, $this->output, $questionInstance);
    }
}
