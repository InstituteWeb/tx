<?php
namespace InstituteWeb\Tx;

use Symfony\Component\Console\Application;

class TxApplication extends Application
{
    public function __construct($name = 'TYPO3 Extension Maker', $version = 'UNKNOWN')
    {
        parent::__construct($name, $version);

        $this->add(new Command\KickstartCommand());
        $this->add(new Command\MinifyCommand());
    }
}
