<?php
namespace InstituteWeb\Tx;

class Variable
{

    /**
     * Returns path to template folder
     *
     * @param string $name If given it appends the name to path
     * @return string
     */
    public static function templatePath($name = '')
    {
        $path = realpath(__DIR__ . '/../templates');
        if ($name) {
            $path .= DIRECTORY_SEPARATOR . $name;
        }
        return $path;
    }


}
