<?php
namespace InstituteWeb\Tx\Value;

/**
 * Class Path
 *
 * @package InstituteWeb\Tx
 */
class Path
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var bool
     */
    private $isExisting = false;

    /**
     * @var bool
     */
    private $parentIsExisting = false;

    /**
     * The appending folder part in path, which does not exist yet
     *
     * @var string
     */
    private $newFolderPart;

    /**
     * Path value object
     *
     * @param string $path Existing path (last directory may be a new one)
     */
    public function __construct($path)
    {
        $this->path = realpath($path);
        if (!$this->path) {
            $pathParts = explode(DIRECTORY_SEPARATOR, trim($path, DIRECTORY_SEPARATOR));
            $this->newFolderPart = array_pop($pathParts);
            $this->path = realpath(implode(DIRECTORY_SEPARATOR, $pathParts));
            if ($this->path) {
                $this->path .= DIRECTORY_SEPARATOR . $this->newFolderPart;
            }
        } else {
            $this->isExisting = true;
        }

        if (!$this->path) {
            $this->path = $path;
            return;
        }
        $this->parentIsExisting = true;
    }

    /**
     * Get Path
     *
     * @return string
     */
    public function get()
    {
        return $this->path;
    }

    /**
     * Get file extension (e.g. ".css")
     *
     * @return string
     */
    public function getFileExtension()
    {
        return pathinfo($this->get(), PATHINFO_EXTENSION);
    }

    /**
     * Get isExisting
     *
     * @return bool
     */
    public function isExisting()
    {
        return $this->isExisting;
    }

    /**
     * Get ParentIsExisting
     *
     * @return boolean
     */
    public function isParentExisting()
    {
        return $this->parentIsExisting;
    }

    /**
     * Get NewFolderPart
     *
     * @return string
     */
    public function getNewFolderPart()
    {
        return $this->newFolderPart;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->path;
    }
}
