<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'InstituteWeb.' . $_EXTKEY,
    'Main',
    'WebManager'
);

if (TYPO3_MODE === 'BE') {
    /**
     * Registers a Backend Module
     */
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'InstituteWeb.' . $_EXTKEY,
        'tools',     // Make module a submodule of 'tools'
        'manamanager',    // Submodule key
        '',                        // Position
        [
            'Project' => 'list, show, new, create, edit, update, delete',
        ],
        [
            'access' => 'user,group',
            'icon' => 'EXT:' . $_EXTKEY . '/ext_icon.gif',
            'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_manamanager.xlf',
        ]
    );
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    $_EXTKEY,
    'Configuration/TypoScript',
    'Mana - Deployment Manager'
);

//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_mana_domain_model_project', 'EXT:mana/Resources/Private/Language/locallang_csh_tx_mana_domain_model_project.xlf');
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mana_domain_model_project');
//
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_mana_domain_model_remote', 'EXT:mana/Resources/Private/Language/locallang_csh_tx_mana_domain_model_remote.xlf');
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mana_domain_model_remote');
//
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_mana_domain_model_buildanddeploylog', 'EXT:mana/Resources/Private/Language/locallang_csh_tx_mana_domain_model_buildanddeploylog.xlf');
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mana_domain_model_buildanddeploylog');
//
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_mana_domain_model_mapping', 'EXT:mana/Resources/Private/Language/locallang_csh_tx_mana_domain_model_mapping.xlf');
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mana_domain_model_mapping');
//
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_mana_domain_model_plan', 'EXT:mana/Resources/Private/Language/locallang_csh_tx_mana_domain_model_plan.xlf');
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mana_domain_model_plan');
//
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_mana_domain_model_task', 'EXT:mana/Resources/Private/Language/locallang_csh_tx_mana_domain_model_task.xlf');
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mana_domain_model_task');
