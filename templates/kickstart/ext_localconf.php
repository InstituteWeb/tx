<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'InstituteWeb.' . $_EXTKEY,
    'Main',
    [
        'WebManager' => 'index',

    ],
    // non-cacheable actions
    [
        'Project' => 'create, update, delete',

    ]
);
